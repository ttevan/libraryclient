import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Customer } from 'src/app/interfaces/customer';
import { LibraryService } from 'src/app/service/library.service';
import { UtilityService } from 'src/app/service/utility.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: string = '';
  pass: string = '';
  message: string = '';

  constructor(private apiService: LibraryService, private utilService: UtilityService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  // --- Metodo para obtener el token
  getToken():void {
    if (this.user != '' && this.pass != '') {
      let bdy = {
        username: this.user,
        password: this.pass
      }
      this.apiService.generateToken('customers/generateToken',bdy)
      .subscribe((token: any) => {
        // --- Compobramo que el token no este vacio
        if (token != null && token != '') {
          // ---Hacemos set del token obtenido
          localStorage.setItem('auth_token', token);
          this.login();
        }else {
          this.message = 'Datos de acceso incorrectos';
        }
      }, (e:any) => console.log(e));
    }else {
      this.message = 'El usuario y la contraseña son obligatorios';
    }
  }

  // --- Metodo para el inicio de sesion
  login() {
    let bdy = {
      username: this.user,
      password: this.pass
    }
    this.apiService.post('customers/login', bdy)
    .subscribe((customer: any) => {
      if (customer != null) {
        this.apiService.setLoggedIn(true);
        this.apiService.setCustomer(customer);
        // this.router.navigate(['/dashboard']);
        this.router.navigate(['/dashboard',{outlets:{dashboardItem:['book']}}]);
      }
    }, (e:any) => console.log(e));
  }
}
