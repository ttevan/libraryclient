import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/interfaces/book';
import { Customer } from 'src/app/interfaces/customer';
import { Shopping } from 'src/app/interfaces/shopping';
import { LibraryService } from 'src/app/service/library.service';
import { UtilityService } from 'src/app/service/utility.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  dataBooks: Book[] = [];
  // -- Cantidad de libros para agregar
  quantity: number = 0;
  // -- Declaracion de las interfaces
  customer: Customer | undefined;
  bookSelected: Book | undefined;
  shopping: Shopping | undefined;
  // -- Bandera para el modal cantidad
  displayDialogModal: boolean = false;

  constructor(private apiService: LibraryService, public utilService: UtilityService) { }

  ngOnInit(): void {
    this.getBooks();
  }

  // -- Metodo para obtener los libros del servicio
  getBooks(): void {
    this.apiService.get('books')
    .subscribe((books: Book[]) => {
      console.log(books);
      if (books.length > 0) { // -- Si existe libros
        this.dataBooks = books;
      }
    }, (e:any) => console.log(e.error));
  }

  // -- Metodo para agregar un libro
  openToAddBook(book: Book):void {
    this.bookSelected = book;
    this.displayDialogModal = true;
  }

  // -- Metodo para guardar un libro en el carrito
  addBook() {
    this.shopping = {
      idBook: this.bookSelected?.id,
      quantity: this.quantity,
      idCustomer: (this.customer?.id ?? 1),
      price: this.bookSelected?.price,
      total: (this.quantity* (this.bookSelected?.price ?? 0))
    }
    this.apiService.post('shoppings/',this.shopping)
    .subscribe((shopping: Shopping) => {
      if (shopping != null) {
        this.quantity = 0;
        this.getBooks();
      }
    }, (e:any) => console.log(e.error))
    this.displayDialogModal = false;
  }
  // --- Metodo para cancelar el agregar libros
  cancelModalBook() {
    this.displayDialogModal = false;
    this.shopping = undefined;
    this.quantity = 0;
  }
}
