import { Component, OnInit } from '@angular/core';
import { LibraryService } from 'src/app/service/library.service';
import { MenuItem } from 'primeng/api';
import { Customer } from 'src/app/interfaces/customer';
import { Book } from 'src/app/interfaces/book';
import { UtilityService } from 'src/app/service/utility.service';
import { Shopping } from 'src/app/interfaces/shopping';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  textWelcomeUser: string = '';
  quantityShop = 0;

  // -- Declaracion de la interfaz customer
  customer: Customer | undefined;

  constructor(private apiService: LibraryService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // --- Recuperamos los datos del cliente
    this.customer = this.apiService.getCustomer();
    this.addTextWelcome();
    this.getQuantityBookShop();
  }

  // -- Metodo para saber si tiene libros en el carrito
  getQuantityBookShop(): void {
    this.apiService.get('shoppings/quantity/'+this.customer?.id)
    .subscribe((quantity: number) => {
      this.quantityShop = quantity;
    }, (e:any) => console.log(e));
  }

  // --- Metodo par ael texto de bienvenida
  addTextWelcome(): void {
    let hours = new Date().getHours();
    if (hours < 12) {
      this.textWelcomeUser = 'Buenos días';
    }else if (hours < 18) {
      this.textWelcomeUser = 'Buenas Tardes';
    }else {
      this.textWelcomeUser = 'Buenas Noches';
    }
  }

  // --- Metodo para cerrar sesión
  signOut():void {
    localStorage.removeItem('auth_token');
    // -- Variable para el guard en las rutas en false
    this.apiService.setLoggedIn(false);
    this.router.navigate(['/']);
  }
}
