import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/interfaces/customer';
import { Shopping } from 'src/app/interfaces/shopping';
import { LibraryService } from 'src/app/service/library.service';
import { UtilityService } from 'src/app/service/utility.service';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.scss']
})
export class ShoppingComponent implements OnInit {

  shoppings: Shopping[] = [];
  shopping: Shopping | undefined;
  displayDialogModal: boolean = false;
  totalPrice: number = 0;
  quantity: number = 0;
  // -- Declaracion de la interfaz customer
  customer: Customer | undefined;

  constructor(private apiService: LibraryService, public utilService: UtilityService) { }

  ngOnInit(): void {
    // --- Recuperamos los datos del cliente
    this.customer = this.apiService.getCustomer();
    this.get();
  }

  // --- Metodo par aobtener todas las compras por consumidor
  get() {
    this.apiService.get('shoppings/byCustomer/'+this.customer?.id)
    .subscribe((res: Shopping[]) => {
      this.shoppings = res;
      res.filter((shopping: Shopping) => this.totalPrice += (shopping.total ?? 0));
    }, (e:any) => console.log(e));
  }

  // --- Metodo para realizar una compra
  buy():void {
    this.apiService.post('shoppings/buy', this.shoppings)
    .subscribe((res: any) => {
      this.get();
    }, (e:any) => console.log(e))
  }

  // -- Metodo para actualizar la cantidad
  edit(shopping: Shopping) {
    this.shopping = shopping;
    this.quantity = (this.shopping.quantity ?? 0);
    this.displayDialogModal = true;
  }

  update() {

  }

  // -- Metodo para remover un item del carrito
  remove(id: number) {
    this.apiService.delete('shoppings/'+id)
    .subscribe((res: Shopping) => {
      this.get();
    }, (e:any) => console.log(e));
  }

  // --- Metodo para cancelar el agregar libros
  cancelModalBook() {
    this.displayDialogModal = false;
    this.shopping = undefined;
    this.quantity = 0;
  }
}
