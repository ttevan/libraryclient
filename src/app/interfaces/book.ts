export interface Book {
    id: number;
    name: string;
    price: number;
    stock: number;
    coverRoute: string;
    description: string;
    status: string;
    createAt: Date;
}
