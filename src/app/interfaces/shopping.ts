import { Book } from "./book";

export interface Shopping {
    id?: number;
    idCustomer?: number;
    idBook?: number;
    price?: number;
    total?: number;
    status?: string;
    quantity?: number;
    book?: Book;
    createAt?: Date;
}
