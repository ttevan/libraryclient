import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(private toastr: ToastrService) {
  }

  public notification(type: string, message: string) {
    if (type == 'success') {
      this.toastr.success(message, '');
    }else if (type == 'error') {
      this.toastr.error(message, '');
    }else if (type == 'info') {
      this.toastr.info(message, '');
    }else if (type == 'warning') {
      this.toastr.warning(message, '');
    }
  }

  miles(num: any) {
    num = String(num).replace(/\D/g, '');
    return (num === '') ? num : Number(num).toLocaleString();
  }

  removeMiles(num: any) {
    // if (num != null && num != undefined && num.includes('.')) {
    if (num != null && num != undefined && num.toString().includes('.')) {
      num = num.replace(/\./g, '');
    }
    if (num != null && num != undefined && num.toString().includes(',')) {
      num = num.replace(/\,/g, '');
    }
    return num;
  }

  validateOnlyNumbers(e: any) {
    var code = e.which || e.keyCode,
    allowedKeys = [8,9,13,27,35,36,37,39,46]; //[8,9,13,27,35,36,37,38,39,46,110,190];
    
    if (allowedKeys.indexOf(code) > -1) {
      return;
    }
    
    if ((e.shiftKey || (code < 48 || code > 57)) && (code < 96 || code > 105)) {
      e.preventDefault();
    }
  }
}
