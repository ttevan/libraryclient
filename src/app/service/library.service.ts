import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Customer } from '../interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  customer: Customer | undefined;
  private loggedIn: boolean = false;

  constructor(private http: HttpClient) { }

  get(path: string): Observable<any> {
    return this.http.get<any>(environment.apiUrl + path);
  }

  post(path: string, data: any) {
    let body = JSON.stringify(data);

    return this.http.post(environment.apiUrl + path, data);
  }

  delete(path: string) {
    return this.http.delete(environment.apiUrl + path)
  }

  setLoggedIn(value: boolean) { this.loggedIn = value; }

  isLoggedIn(): boolean { return this.loggedIn; }

  setCustomer(customer: Customer) {
    this.customer = customer;
  }

  getCustomer() {
    return this.customer;
  }

  generateToken(path: string, data: any) {
    return this.http.post(environment.apiUrl + path, data, {responseType: 'text' as 'json'});
  }
}
