import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LibraryService } from '../service/library.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private apiService: LibraryService, private _router: Router) {}

  canActivate() {
    // --- Si la variable isLogged esta en false; vuelve a iniciar sesion
    if (!this.apiService.isLoggedIn()) {
      // navigate to login page
      this._router.navigate(['/']);
      return false;
    }

    return true;
  }
  
}
